package com.haojiangbo.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("service.magic")
public class ServiceMagicProperties {
    private String callPrefix = "Call";


    public String getCallPrefix() {
        return callPrefix;
    }

    public void setCallPrefix(String callPrefix) {
        this.callPrefix = callPrefix;
    }
}
