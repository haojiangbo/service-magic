package com.haojiangbo.config;

import com.haojiangbo.config.mapping.RequestMappingConfig;
import com.haojiangbo.config.properties.ServiceMagicProperties;
import com.haojiangbo.config.result.ServiceMagicResultHander;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * 自动配置
 * @author 郝江波
 * @date 2021/2/8 10:10
 *
 */
@EnableConfigurationProperties({ServiceMagicProperties.class})
public class ServiceMagicAutoConfig {

    @Bean
    public RequestMappingConfig requestMappingConfig() {
        return new RequestMappingConfig();
    }

    @Bean
    public ServiceMagicResultHander serviceMagicResultHander() {
        return new ServiceMagicResultHander();
    }
}
