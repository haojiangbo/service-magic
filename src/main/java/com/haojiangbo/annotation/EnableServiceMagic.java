package com.haojiangbo.annotation;
import com.haojiangbo.config.ServiceMagicAutoConfig;
import org.springframework.context.annotation.Import;
import java.lang.annotation.*;
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({ServiceMagicAutoConfig.class})
public @interface EnableServiceMagic {
}
